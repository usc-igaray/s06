from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod
    def get_fullName(self):
        pass
    
    @abstractclassmethod
    def add_request(self):
        pass
    
    @abstractclassmethod
    def check_request(self, request):
        pass
    
    @abstractclassmethod
    def add_user(self):
        pass
    
class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    
    def set_firstName(self, firstName):
        self._firstName = firstName
    def get_firstName(self):
        # print(f'First name of Employee: {self._firstName}')
        return self._firstName
    
    def set_lastName(self, lastName):
        self._lastName = lastName
    def get_lastName(self):
        # print(f'Last name of Employee: {self._lastName}')
        return self._lastName
    
    def set_email(self, email):
        self._email = email
    def get_email(self):
        print(f'Email of Employee: {self._email}')
        # return self._email
    
    def set_department(self, department):
        self._department = department
    def get_department(self):
        # print(f'Department of Employee: {self._department}')
        return self.department
    
    # Person abstract methods
    def get_fullName(self):
        # print(f"Full name of Employee: {self._firstName} {self._lastName}")
        return f"{self._firstName} {self._lastName}"
    def add_request(self):
        return "Request has been added"
    def check_request(self, request):
        pass
    def add_user(self, user):
        pass
    
    # Employee abstract methods
    def login(self):# outputs "<Email> has logged in"
        # print(f"{self._email} has logged in")
        return f"{self._email} has logged in"

    # @abstractclassmethod
    def logout(self): # outputs "<Email> has logged out"
        # print(f"{self._email} has logged out")
        return f"{self._email} has logged out"
    
class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = []
    
    def set_firstName(self, firstName):
        self._firstName = firstName
    def get_firstName(self):
        # print(f'First name of Employee: {self._firstName}')
        return self._firstName
    
    def set_lastName(self, lastName):
        self._lastName = lastName
    def get_lastName(self):
        # print(f'Last name of Employee: {self._lastName}')
        return self._lastName
    
    def set_email(self, email):
        self._email = email
    def get_email(self):
        # print(f'Email of Employee: {self._email}')
        return self._email
    
    def set_department(self, department):
        self._department = department
    def get_department(self):
        # print(f'Department of Employee: {self._department}')
        return self.department
    
   
    def get_members(self):
        return self._members
    
    # Person abstract methods
    def get_fullName(self):
        # print(f"Full name of Employee: {self._firstName} {self._lastName}")
        return f"{self._firstName} {self._lastName}"
    def add_request(self, request):
        pass
    def check_request(self, request):
        pass
    def add_user(self, user):
        pass
    
    # TeamLead abstract methods
    def login(self):# outputs "<Email> has logged in"
        # print(f"{self._email} has logged in")
        return f"{self._email} has logged in"

    # @abstractclassmethod
    def logout(self): # outputs "<Email> has logged out"
        # print(f"{self._email} has logged out")
        return f"{self._email} has logged out"
    # @abstractclassmethod
    def add_member(self, employee): # add employee to the members list
        self._members.append(employee)

class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
    
    def set_firstName(self, firstName):
        self._firstName = firstName
    def get_firstName(self):
        # print(f'First name of Employee: {self._firstName}')
        return self._firstName
    
    def set_lastName(self, lastName):
        self._lastName = lastName
    def get_lastName(self):
        # print(f'Last name of Employee: {self._lastName}')
        return self._lastName
    
    def set_email(self, email):
        self._email = email
    def get_email(self):
        # print(f'Email of Employee: {self._email}')
        return self._email
    
    def set_department(self, department):
        self._department = department
    def get_department(self):
        # print(f'Department of Employee: {self._department}')
        return self.department
    
    # Person abstract methods
    def get_fullName(self):
        # print(f"Full name of Employee: {self._firstName} {self._lastName}")
        return f"{self._firstName} {self._lastName}"
    def add_request(self, request):
        pass
    def check_request(self, request):
        pass
    def add_user(self):# outputs new user added
        return "User has been added"
    
    # Employee abstract methods
    # @abstractclassmethod
    def login(self):# outputs "<Email> has logged in"
        # print(f"{self._email} has logged in")
        return f"{self._email} has logged in"

    # @abstractclassmethod
    def logout(self): # outputs "<Email> has logged out"
        # print(f"{self._email} has logged out")
        return f"{self._email} has logged out"
              
class Request():
    def __init__(self, name, requester, dateRequested):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = ""
     
    def set_status(self, status):
        self._status = status
        
    def update_request(self):
        return f"Request {self._name} has been updated"
        
    def close_request(self):
        return f"Request {self._name} has been closed"
        
    def cancel_request(self):
        return f"Request {self._name} has been canceled"

employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing" )
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.get_fullName() == "John Doe", "Full name should be John Doe"
assert admin1.get_fullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.get_fullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.add_request() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.add_member(employee3)
teamLead1.add_member(employee4)

for indiv_empl in teamLead1.get_members():
    print(indiv_empl.get_fullName())
    
assert admin1.add_user() == "User has been added"
req2.set_status("closed")
print(req2.close_request())